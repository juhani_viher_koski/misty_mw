OBJS=server.o listener.o dbpool.o sha2big/sha2big.o
H=server.h dbpool.h
LIBS= -lpq -pthread
EXE=spruced
CC=gcc
CFLAGS=-std=gnu99 -Wall -Wno-deprecated-declarations -g -O2 -march=native -Isha2big 

server: $(OBJS) 
	gcc -std=gnu99 $(CFLAGS) -o $(EXE) $(OBJS) $(LIBS)

server.o: server.h dbpool.h
listener.o: server.h dbpool.h
dbpool.o: dbpool.h

clean:
	rm -f $(OBJS) $(EXE)
