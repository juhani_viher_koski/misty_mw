#! /bin/sh

function hangup()
{
	echo "Received a signal, exiting" >&2
	echo LOGOUT
	exit 1
}
trap hangup SIGINT SIGHUP SIGTERM

id=$1
pass=foobarbaz

read auth salt

if test AUTH != "$auth"; then
	echo No auth token >&2
	exit 1
fi

token=$(./shacl $id $pass $salt)
echo LOGIN "$id" "$token"

read response
if test OK != "$response"; then
	echo Invalid login >&2
	exit 1
fi

echo "UPDATE $id $id"
echo "RANGE 0 0 100 100"
sleep 1
echo "QUERY"
sleep 1
echo LOGOUT
