#define _GNU_SOURCE 

#include <sys/socket.h>
#include <sys/select.h>
#include <poll.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <unistd.h>
#include <libpq-fe.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <locale.h>
#include <pthread.h>

#include "dbpool.h"

dbpool* dbpool_init(int num_of_conns, char* conninfo) 
{
	dbpool* p = malloc(sizeof(dbpool));
	if(p == NULL) {
		return NULL;
	}
	p->num_conns = num_of_conns ? num_of_conns : DBPOOL_INITIAL_SIZE;
	p->active_conns = 0;
	p->conns = malloc(sizeof(p->conns)*p->num_conns);
	if(p->conns == NULL) {
		free(p);
		return NULL;
	}
	p->conn_active = malloc(sizeof(p->conn_active)*p->num_conns);
	if(p->conn_active == NULL) {
		free(p->conns);
		free(p);
		return NULL;
	}
	for (int t=0; t < p->num_conns; t++) {
		PGconn* tmp = PQconnectdb(conninfo);
		if(tmp == NULL) {
			for(int i=t-1; i > 0; i--) {
				PQfinish(p->conns[i]);
			}
			free(p->conns);
			free(p->conn_active);
			free(p);
			return NULL;
		}
		p->conn_active[t] = 0;
		p->conns[t] = tmp;
	}
	pthread_mutex_init(&(p->lock), NULL);
	pthread_mutex_init(&(p->acr), NULL);
	return p;
}
	
dbconn* dbpool_get(dbpool* p) 
{
	int available = 0, current = -1;
	PGconn* c = NULL;
retry:
	pthread_mutex_lock(&p->lock);
	for(int t=0; t < p->num_conns; t++) {
		if(p->conn_active[t] == 0) {
			available++;
			current = t;
			c = p->conns[current];
		}
	}
	if(current != -1) {
		p->conn_active[current] = 1;
	} else {
		pthread_mutex_unlock(&p->lock);
		usleep(1100);
		goto retry;
	}
	pthread_mutex_unlock(&p->lock);
	dbconn* tmp = malloc(sizeof(dbconn));
	if(tmp == NULL) {
	// FIXME abort() the program instead...
		return NULL;
	}
	tmp->c = c;
	tmp->conn_num = current;
	return tmp;
}

void dbpool_release(dbpool* p, dbconn* c) 
{
	pthread_mutex_lock(&p->lock);
	p->conn_active[c->conn_num] = 0;
	pthread_mutex_unlock(&p->lock);
	free(c);
}
