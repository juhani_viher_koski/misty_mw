typedef struct {
	PGconn** conns;
	int* conn_active;
	int num_conns;
	int active_conns;
	pthread_mutex_t lock;
	pthread_mutex_t acr;
} dbpool;

typedef struct {
	PGconn* c;
	int conn_num;
} dbconn;

#define DBPOOL_INITIAL_SIZE 4

dbpool* dbpool_init(int num_of_conns, char* conninfo);
dbconn* dbpool_get(dbpool* p);
void dbpool_release(dbpool* p, dbconn* c);

