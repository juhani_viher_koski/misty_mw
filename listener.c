#include <sys/socket.h>
#include <sys/select.h>
#include <poll.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <unistd.h>
#include <libpq-fe.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <locale.h>
#include <pthread.h>
#include <signal.h>
#include <malloc.h>

#include "dbpool.h"
#include "server.h"

#define SERVER_PORT 32000

pthread_mutex_t memmutex;

// for saving old hooks
void* (*old_malloc_hook) (size_t, const void*);
void* (*old_realloc_hook) (void*, size_t, const void*);
void (*old_free_hook) (void*, const void*);


void* mt_malloc(size_t size, const void* caller);
void* mt_realloc(void* ptr, size_t size, const void* caller);
void mt_free(void* ptr, const void* caller);

void mt_init_hook(void)
{
	pthread_mutex_init(&memmutex, NULL);
	old_malloc_hook = __malloc_hook;
	old_realloc_hook = __realloc_hook;
	old_free_hook = __free_hook;
	__malloc_hook = mt_malloc;
	__free_hook = mt_free;
}

// Override initializing hook from the glibc
// void (*__MALLOC_HOOK_VOLATILE __malloc_initialize_hook) (void) = mt_init_hook;

void* mt_malloc(size_t size, const void* caller)
{
	pthread_mutex_lock(&memmutex);
	// restore the old hook
	__malloc_hook = old_malloc_hook;
	void* tmp = malloc(size);
	// restore our handler
	__malloc_hook = mt_malloc;
	pthread_mutex_unlock(&memmutex);
	return tmp;
}

void* mt_realloc(void* ptr, size_t size, const void* caller)
{
	pthread_mutex_lock(&memmutex);
	__realloc_hook = old_realloc_hook;
	void* tmp = realloc(ptr, size);
	__realloc_hook = mt_realloc;
	pthread_mutex_unlock(&memmutex);
	return tmp;
}

void mt_free(void* ptr, const void* caller)
{
	pthread_mutex_lock(&memmutex);
	__free_hook = old_free_hook;
	free(ptr);
	__free_hook = mt_free;
	pthread_mutex_unlock(&memmutex);
}

int main(void)
{
	mt_init_hook();
	dbpool* pool = dbpool_init(16, "");
	if(pool == NULL) {
		puts("dbpool");
		exit(1);
	}
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock == -1) {
		puts("socket");
		exit(1);
	}
	struct sockaddr_in myaddr, claddr;
	memset(&myaddr, 0, sizeof(myaddr));
	myaddr.sin_family = AF_INET;
	myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	myaddr.sin_port = htons(SERVER_PORT);
	int b = bind(sock, (struct sockaddr*) &myaddr, sizeof(myaddr));
	if(b == -1) {
		puts("bind()");
		exit(1);
	}
	listen(sock, 4096);
	while(1) {
		socklen_t len = sizeof(claddr);
		pthread_t thread;
		int connfd = accept(sock, (struct sockaddr*) &claddr, &len);
		tdata* d = malloc(sizeof(tdata));
		d->fd = connfd;
		d->p = pool;
		if(pthread_create(&thread, NULL, thread_main, (void*) d) ) {
			puts("Thread creation failed!");
			close(connfd);
			free(d);
		}
	}
	return 0;
}
	
