/* a simple thread handler */

#define _GNU_SOURCE 

#include <sys/socket.h>
#include <sys/select.h>
#include <poll.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <unistd.h>
#include <libpq-fe.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <locale.h>

#include "dbpool.h"
#include "server.h"
#include "sph_sha2.h"

// maximum packet size 
#define BUFLEN 65536
#define IDMAXLEN 128
// server port
#define PORT 32000

#define IPACKET -1
#define OK 0
#define NOK -1
#define LOGOUT 2
#define IOERROR 3

// set mobile's location on the globe
int db_update(tdata* d, double x, double y)
{
	d->x = x; d->y = y;
	char str[1024];
	snprintf(str, 1023, 
		"UPDATE mobiles SET location"
		"=point(%.12f, %.12f) WHERE id=%s;", x, y, d->id);
	dbconn* conn = dbpool_get(d->p);
	PGresult *r = PQexec(conn->c, str);
	dbpool_release(d->p, conn);
	PQclear(r);
	return OK;
}

// find nearest neighbors for a mobile
int db_query(tdata* d) 
{
	int retval;
	if(d->x == -1 && d->y == -1) { 
		// client has not set correct location data yet, disconnect
		fputs("RESULTS 0 LOCATION_NOT_SET\n", d->f);
		return NOK;
	}
	char str[1024];
	snprintf(str, 1023, 
		"SELECT id, location FROM mobiles "
		"WHERE id <> %s "
		"ORDER BY location <-> point (%.12f, %.12f)"
		" LIMIT 10;", d->id, d->x, d->y);
	dbconn* conn = dbpool_get(d->p);
	PGresult *r = PQexec(conn->c, str);
	dbpool_release(d->p, conn);
	retval = fprintf(d->f, "RESULTS %d DATA\n", PQntuples(r));
	if(retval < 0) { retval = IOERROR; goto cleanup; }
	for(int i=0; i<PQntuples(r); i++) {
		char* nid = PQgetvalue(r, i, 0);
		double nx, ny;
		sscanf(PQgetvalue(r, i, 1), "(%lf,%lf)", &nx, &ny);
		retval = fprintf(d->f, "%d %s %.12f %.12f\n", i, nid, nx, ny);
		if(retval < 0) { retval = IOERROR; goto cleanup; }
	}
	retval = OK;
cleanup:
	PQclear(r);
	return retval;
}

// find neighbors for a mobile in a specified range
int db_range(tdata *d, double xs, double ys, double xe, double ye)
{
	int retval;
	char str[1024];
	snprintf(str, 1023,
		"SELECT id, location FROM mobiles "
		"WHERE location <@ box\'((%.12f,%.12f),(%.12f,%.12f))\';"
		, xs, ys, xe, ye);
	dbconn* conn = dbpool_get(d->p);
	PGresult* r = PQexec(conn->c, str);
	dbpool_release(d->p, conn);
	retval = fprintf(d->f, "RESULTS %d DATA\n", PQntuples(r));
	if(retval < 0) { retval = IOERROR; goto cleanup; }
	for(int i=0; i<PQntuples(r); i++) {
		char* nid = PQgetvalue(r, i, 0);
		double nx, ny;
		sscanf(PQgetvalue(r, i, 1), "(%lf,%lf)", &nx, &ny);
		retval = fprintf(d->f, "%d %s %.12f %.12f\n", i, nid, nx, ny);
		if(retval < 0) { retval = IOERROR; goto cleanup; }
	}
	retval = OK;
cleanup:
	PQclear(r);

	return OK;
}

int db_check(tdata* d, char** arg, const int count)
{
	int found[count];
	dbconn* conn = dbpool_get(d->p);
	for(int t=0; t < count; t++) {
		char* id = arg[t];
		char str[1024];
		snprintf(str, 1023,
			"SELECT COUNT(id) FROM mobiles "
			"WHERE id = '%s';", id);
		PGresult* r = PQexec(conn->c, str);
		char* val = PQgetvalue(r, 0, 0);
		found[t] = (val[0] == '1') ? 1 : 0;
		PQclear(r);
	}
	dbpool_release(d->p, conn);
	fprintf(d->f, "RESULTS %d DATA\n", count);
	for(int t=0; t < count; t++) {
		int retval = fprintf(d->f, "%d %s\n", t, found[t] ? "YES" : "NO");
		if(retval < 0) { return IOERROR; }
	}
	return OK;
}		

// parse commands and call action functions
int event_process(tdata* d, char* buf)
{
	if(! strcmp(buf, "QUERY")) {
		return db_query(d);
	} else if(! strncmp(buf, "UPDATE ", 7)) {
		char* arg1 = &buf[7];
		char* arg1end = strchr(&buf[7], ' ');
		if(arg1end == NULL) { return NOK; }
		*arg1end = '\0';
		char* arg2 = &arg1end[1];
		char* arg2end = strchr(arg2, '\0');
		if(arg2end == NULL) { return NOK; }
		double x = atof(arg1);
		double y = atof(arg2);
		return db_update(d, x, y);
	} else if(! strncmp(buf, "RANGE ", 6)) {
		char* arg1 = &buf[6];
		char* arg1end = strchr(&buf[7], ' ');
		if(arg1end == NULL) { return NOK; }
		*arg1end = '\0';
		char* arg2 = &arg1end[1];
		char* arg2end = strchr(arg2, ' ');
		if(arg2end == NULL) { return NOK; }
		*arg2end = '\0';
		char* arg3 = &arg2end[1];
		char* arg3end = strchr(arg3, ' ');
		if(arg3end == NULL) { return NOK; }
		*arg3end = '\0';
		char* arg4 = &arg3end[1];
		char* arg4end = strchr(arg4, '\0');
		if(arg4end == NULL) { return NOK; }
		*arg4end = '\0';
		double xs = atof(arg1);
		double ys = atof(arg2);
		double xe = atof(arg3);
		double ye = atof(arg4);
		return db_range(d, xs, ys, xe, ye);
	} else if(! strncmp(buf, "CHECK ", 6)) {
		char* argbegin = &buf[6];
		int argnum;
		char* arglist[10];
		for(argnum=0; argnum<10; argnum++) {
			arglist[argnum] = argbegin;
			char* argend = strchr(argbegin, ' ');
			if(argend == NULL) {
				break;
			}
			*argend = '\0';
			argbegin = argend+1;
		}
		return db_check(d, arglist, argnum+1);
	} else if(! strncmp(buf, "LOGOUT", 6)) {
		return LOGOUT;
	}
	return NOK;	
}

void bin2hex(char* src, char* dst, int len)
{
	const char hex[] = "0123456789abcdef";
	for(int t=0; t<len; t++) {
		unsigned char lsn = src[t] & 0x0F;
		unsigned char msn = (src[t] & 0xF0) >> 4;
		dst[t*2] = hex[msn];
		dst[t*2+1] = hex[lsn];
	}
	dst[len*2] = '\0';
}

// check login credentials. 
int login_process(tdata* d, char* buf, char* salt)
{
	int retval;

	if(! strcmp(buf, "REQUEST")) {
		// client asks for a user id/password
		// generate a random password
		int fd = open("/dev/urandom", O_RDONLY);
		if(fd == -1) { return IOERROR; }
		char res[8];
		char password[17];
		if(read(fd, res, 8) != 8) { return IOERROR; }
		bin2hex(res, password, 8);
		close(fd);
		char cmd[1024];
		snprintf(cmd, 1023, "INSERT INTO users (password)" 
			" VALUES('%s') RETURNING userid;", password);
		dbconn* conn = dbpool_get(d->p);
		PGresult *r = PQexec(conn->c, cmd);
		dbpool_release(d->p, conn);
		char* val = PQgetvalue(r, 0, 0);
		fprintf(d->f, "REPLY %s %s\n", val, password);
		fflush(d->f);
		PQclear(r);
		return NOK; // log out; client then tries again with LOGIN
	}

	// parse LOGIN command
	char* cmdend = strchr(buf, ' ');
	if(cmdend == NULL || strncmp(buf, "LOGIN", 5)) { return NOK; }
	*cmdend = '\0';
	char* par1 = cmdend+1;
	char* par1end = strchr(par1, ' ');
	if(par1end == NULL) { return NOK; }
	if((par1end - par1) > 22) { return NOK; }
	*par1end = '\0';
	char* par2 = par1end+1;
	char* par2end = strchr(par2, ' ');
	if(par2end == NULL) { return NOK; }
	if((par2end - par2) > 128) { return NOK; }
	char* par3 = par2end+1;
	char* par3end = strchr(par3, '\0');
	if(par3end == NULL) { return NOK; }
	
	// check the login credentials
	char cmd[1024];
	snprintf(cmd, 1023, "SELECT password FROM users WHERE userid = %s;", par1); 
	dbconn* conn = dbpool_get(d->p);
	PGresult *r = PQexec(conn->c, cmd);
	dbpool_release(d->p, conn);
	char* password = PQgetvalue(r, 0, 0);
	sph_sha512_context hcontext;
	sph_sha512_init(&hcontext);
	sph_sha512(&hcontext, par2, par2end - par2);
	sph_sha512(&hcontext, password, strlen(password));
	sph_sha512(&hcontext, salt, strlen(salt));
	PQclear(r);
	char res[64];
	sph_sha512_close(&hcontext, res);
	char hexres[129];
	bin2hex(res, hexres, 64);
	if(strcmp(par2, hexres)) { return NOK; }
	d->x = -1; d->y = -1;
	conn = dbpool_get(d->p);
	d->id = PQescapeLiteral(conn->c, par1, par1end - par1);
	snprintf(cmd, 1023, "INSERT INTO mobiles VALUES (%s, point(-1,-1));", d->id);
	r = PQexec(conn->c, cmd);
	dbpool_release(d->p, conn);
	PQclear(r);
	retval = fputs("OK\n", d->f);
	if(retval == EOF) { return NOK; }
	retval = fflush(d->f);
	if(retval == EOF) { return NOK; }

	return OK;
}
	
// reads a command line from stdin
int readline(tdata* d, char* buf, int len)
{
	int a, i = 0;
	struct pollfd pfd;
	pfd.fd = d->fd;
	pfd.events = POLLIN|POLLRDHUP;
	poll(&pfd, 1, 500*1000); 
	if(pfd.revents != POLLIN) {
		return NOK;
	}
	while( (a = getc(d->f)) != EOF) {
		if(a == '\n') {
			buf[i] = '\0';
			return OK;
		}
		buf[i] = a;
		i++;
		if(i == len) { return NOK; } // FIXME buffer overflow about to happen, log this! 
	}
	return NULL;
}

void* thread_main(void* data) { 
	tdata* d = (tdata*) data;
	char buf[BUFLEN];
	
	struct linger c_timeout;
	c_timeout.l_onoff = 1;
	c_timeout.l_linger = 0;
	setsockopt(d->fd, SOL_SOCKET, SO_LINGER, &c_timeout, sizeof(c_timeout));
	d->f = fdopen(d->fd, "w+");
	char token[] = "foob";
	fprintf(d->f, "AUTH %s\n", token);
	fflush(d->f);
	struct pollfd pfd;
	pfd.fd = d->fd;
	pfd.events = POLLIN;
	poll(&pfd, 1, 30*1000);
	if (pfd.revents != POLLIN) { goto login_failed; }
	int retval = readline(d, buf, BUFLEN);
	if(retval != OK) { goto login_failed; }
	if(login_process(d, buf, token) != OK) { 
		goto login_failed;
	}
	while(1) {
		int retval = readline(d, buf, BUFLEN);
		if(retval != OK) { break; }
		int t = event_process(d, buf);
		if(t == IOERROR) { break; }
		if(t == LOGOUT) { break; }
		retval = fflush(d->f);
		if(retval == EOF) { break; }
	}
	char cmd[1024];
	snprintf(cmd, 1023, "DELETE FROM mobiles where id=%s;", d->id);
	dbconn* conn = dbpool_get(d->p);
	PGresult* r = PQexec(conn->c, cmd);
	PQclear(r);
	dbpool_release(d->p, conn);
	free(d->id); // not allocated unless login was successful

login_failed:
	fflush(d->f);
	shutdown(d->fd, 2);
	fclose(d->f);
	free(d);
	return 0;
}

