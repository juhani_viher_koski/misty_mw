void* thread_main(void* fd);

typedef struct {
	int fd; // file handle
	FILE* f;
	dbpool* p;
	char* id;
	double x;
	double y;
} tdata;

// #define DEBUG_PRINTS

#ifdef DEBUG_PRINTS
#define debug_puts(a) puts(a)
#else
#define debug_puts(a)
#endif

