#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sph_sha2.h"

void bin2hex(char* src, char* dst, int len)
{
	const char hex[] = "0123456789abcdef";
	for(int t=0; t<len; t++) {
		unsigned char lsn = src[t] & 0x0F;
		unsigned char msn = (src[t] & 0xF0) >> 4;
		dst[t*2] = hex[msn];
		dst[t*2+1] = hex[lsn];
	}
	dst[len*2] = '\0';
}


int main(int argc, char** argv)
{
	if(argc != 4) {
		puts("Error");
		return 1;
	}
	char *id = argv[1];
	char *password = argv[2];
	char *salt = argv[3];
	int hashlen = strlen(salt)+strlen(id)+strlen(password);
	char hash[hashlen+1]; 
	hash[0] = '\0';
	strcat(hash, id);
	strcat(hash, password);
	strcat(hash, salt);
	sph_sha512_context hcontext;
	sph_sha512_init(&hcontext);
	sph_sha512(&hcontext, hash, hashlen);
	char res[64];
	sph_sha512_close(&hcontext, res);
	char hexres[129];
	bin2hex(res, hexres, 64);
	puts(hexres);
	return 0;
}
